# Redis Cluster Setting

### 1. kubernetes-redis-cluster-master 디렉토리 세팅 
### 2. deployment-configmaps 디렉토리에서 redis server configmap yaml 6개 확인 
```
apiVersion: v1
kind: ConfigMap
metadata:
  name: redis-conf-X
data:
  redis.conf: |+
    bind 0.0.0.0
    cluster-enabled yes
    appendonly no
    protected-mode no
    cluster-config-file /var/lib/redis/nodes.conf
    cluster-node-timeout 5000
    dir /var/lib/redis
    port 6379
    cluster-announce-port 3010X # 외부에서 붙는 포트번호
    cluster-announce-bus-port 3020X	
    cluster-announce-ip 172.168.0.106 # 외부에서 붙는 호스트 ip
```
### 3. persistentVolumeClaim 디렉토리에서 volume claim yaml 6개 확인 
```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: redis-data-redis-app-X
  labels:
    type: redis-nfs-claim
    app: redis
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
  selector:
    matchLabels:
      path: nfsshare1
      app: redis
      podindex: "0"
```
### 4. deployment-services 디렉토리에서 service yaml 6개 확인 
```
apiVersion: v1
kind: Service
metadata:
  name: "redis-X"
spec:
  selector:
    app: "redis"
    name: "redis-X"
  type: NodePort
  ports:
    - name: "redis"
      protocol: "TCP"
      port: 6379
      targetPort: 6379
      nodePort: 3010X
    - name: "cluster"
      protocol: "TCP"
      port: 16379
      targetPort: 16379
      nodePort: 3020X
  clusterIP: "10.96.0.2" # 끝자리만 1씩 늘려가며 생성
```
### 5. deployment 디렉토리에서 deployment pod 리소스 생성 yaml 6개 확인 
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: "redis-X"
  labels:
    app: redis
spec:
  replicas: 1
  selector:
    matchLabels:
      app: redis
  template:
    metadata:
      labels:
        app: redis
        name: "redis-X"
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: app
                  operator: In
                  values:
                  - redis
              topologyKey: kubernetes.io/hostname
      containers:
        - name: redis
          image: "redis:5.0-rc"
          resources:
            requests:
              cpu: "100m"
              memory: "100Mi"
          ports:
            - name: redis
              containerPort: 6379
              protocol: "TCP"
            - name: cluster
              containerPort: 16379
              protocol: "TCP"
          command: ["redis-server", "/etc/redis/redis.conf"]
          volumeMounts:
            - name: "redis-conf"
              mountPath: "/etc/redis"
            - name: "redis-data"
              mountPath: "/var/lib/redis"
      imagePullSecrets:
        - name: regsecret
      volumes:
        - name: "redis-data"
          persistentVolumeClaim:
            claimName: redis-data-redis-app-X
        - name: "redis-conf"
          configMap:
            name: "redis-conf-X"
            items:
              - key: "redis.conf"
                path: "redis.conf"
```
### 6. 메인 디렉토리로 돌아와서 deployment-setup.sh 실행 
```
./deployment-setup.sh
```
### 7. Pod ip 6개로 레디스 클러스터 생성 
```
kubectl exec -it redis-1-675cb4c9b bash 
```
```
redis-cli --cluster create 172.17.0.6:6379 172.17.0.5:6379 172.17.0.4:6379 172.17.0.7:6379 172.17.0.8:6379 172.17.0.9:6379 --cluster-replicas 1
```
